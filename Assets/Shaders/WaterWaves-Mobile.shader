﻿Shader "Custom/WaterWaves-Mobile" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_WaveLength ("Wave Length", Float) = 0.1
		_WaveSpeed ("Wave Speed", Float) = 0.1
		_WaveDisplacementX ("Wave Displacement X",Float) = 0.1
		_WaveDisplacementY ("Wave Displacement Y",Float) = 0.1
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent"}
		LOD 200
		
			Blend SrcAlpha OneMinusSrcAlpha
			Zwrite Off
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _WaveLength,_WaveSpeed, _WaveDisplacementX, _WaveDisplacementY;
			
			struct v2f{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD;
			};
			
			v2f vert (appdata_base v){
				v2f o;
				float4 pos = mul(_Object2World,v.vertex);
				float timeVariance = sin(_Time.y+pos.z);
				float sinCoeff = 1+sin(timeVariance*_WaveSpeed+pos.x*_WaveLength);
				pos.y += _WaveDisplacementY*sinCoeff;
				
				o.pos = mul(UNITY_MATRIX_VP,pos);
				o.uv = TRANSFORM_TEX(v.texcoord,_MainTex);
				
				return o;
			}
			
			half4 frag( v2f i) :COLOR{
				return tex2D(_MainTex,i.uv);
			}
			
			ENDCG
		}
		
	} 
	FallBack "Diffuse"
}
