﻿using UnityEngine;
using System.Collections;

public class DestroyOnEnd : MonoBehaviour {
    public void DestroyInstance(){
        Destroy(gameObject);
    }
}