﻿using UnityEngine;
using System.Collections;

public class CharacterSounds : MonoBehaviour {

    public AudioClip jumpOnboard,kickOffboard,splash0,splash1,splash2,splash3,landOnBoat;

    public void JumpUp(){
        audio.Stop();
        audio.clip = jumpOnboard;
        audio.Play();
    }

    public void JumpDown(){
        audio.Stop();
        audio.clip = kickOffboard;
        audio.Play();
    }

    public void Splash(){
        audio.Stop();
        int val = Random.Range(0,4);

        switch(val){
            case 0: audio.clip = splash0; break;
            case 1: audio.clip = splash1; break;
            case 2: audio.clip = splash2; break;
            case 3: audio.clip = splash3; break;
        }

        audio.Play();
    }

    public void Land(){
        audio.Stop();
        audio.clip = landOnBoat;
        audio.Play();
    }
}
