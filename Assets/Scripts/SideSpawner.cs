﻿using UnityEngine;
using System.Collections;

public class SideSpawner : MonoBehaviour {

    public float spawnInterval;
    public string[] castawayNames;
    public GameObject[] castawayPrefabs;
    Transform[] spawnPoints;
    float currentTimeLeft = 0;
    static SideSpawner instance;
    void Awake() {
        instance = this;
        spawnPoints = new Transform[transform.childCount];

        for (int s = 0; s < spawnPoints.Length; s++) {
            spawnPoints [s] = transform.GetChild(s);
        }
    }
    
    void Update() {
        if (!Lifeboat.IsSinked && GameMaster.currentGameMode == GameMaster.GameMode.Gameplay) {
            currentTimeLeft += Time.deltaTime;

            if (currentTimeLeft > spawnInterval) {
                SpawnCastaway();
                currentTimeLeft -= spawnInterval;
            }
        }
    }

    void SpawnCastaway() {
        Vector3 spawnPosition = spawnPoints [Random.Range(0, spawnPoints.Length)].position;
        string newName = castawayNames [Random.Range(0, castawayNames.Length)];

        GameObject castaway = (GameObject)Instantiate(castawayPrefabs [Random.Range(0, castawayPrefabs.Length)], spawnPosition, Quaternion.identity);

        castaway.name = newName;
        Castaway bhv = castaway.GetComponent<Castaway>();
        if (nextSpawnWithPlanks) {
            bhv.GivePlanks();
            nextSpawnWithPlanks = false;
        }
    }

    static bool nextSpawnWithPlanks = false;

    public static void SpawnPlanks() {
        nextSpawnWithPlanks = true;
    }

    public static void FirstSpawn(){
        instance.currentTimeLeft = 0;
        instance.SpawnCastaway();
    }
}
