using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Castaway : MonoBehaviour {

    [Header("Stats")]
    public int
        roomRequirement;
    public int weight;
    public CastawayStatus castawayStatus;
    public int waterRemoval;
    public int maxEnergyOnboard;
    public int maxEnergySwimming;
    public int currentEnergy;
    public bool consumesEnergyOnboard;
    public bool carriesPlanks;
    public bool resting;
    [Header("Visual Stuff")]
    public float
        swimSpeed;
    public SpriteRenderer sprite;
    public SpriteRenderer outline;
    public SpriteRenderer planks;
    public AnimationCurve jumpCurve;
    public GameObject waterSplash;
    public Animator animator;
    [Header("UIValues")]

    public Text
        nameDisplay;
    public Image energyFill;
    public CanvasGroup canvasGroup;
    public Gradient swimmingGradient;
    public Gradient workingGradient;
    public Gradient restingGradient;

    int restedTimes = 0;
    float xTargetPosition;
    CrewSlot slot;
    bool showInfo;
    bool flaggedForRemoval = false;

    float energyRecovery { get { return 1; } }

    void Awake() {
        currentEnergy = maxEnergySwimming;
        sprite.sortingLayerName = "Foreground";
        animator = GetComponent<Animator>();
    }

    void OnMouseEnter() {
        if (!Lifeboat.IsSinked) {
            outline.enabled = true;
            outline.color = Color.green;
            if (castawayStatus == CastawayStatus.Rescued)
                showInfo = true;
        }
    }

    void OnMouseDown() {
        if (!Lifeboat.IsSinked) {
            switch (castawayStatus) {
                case CastawayStatus.Swimming:
                    if (Lifeboat.RoomLeft >= roomRequirement) {
                        SetStatus(CastawayStatus.Rescued);
                        GameMaster.peopleRescued++;
                    } else {
                        Debug.Log("Not enough room!!");
                    }

                    break;
                case CastawayStatus.Rescued:
                    if (!flaggedForRemoval) {
                        outline.color = Color.red;
                        flaggedForRemoval = true;
                    } else {
                        SetStatus(CastawayStatus.Rejected);
                        GameMaster.peopleKicked++;
                    }
                    break;
            }
        }
    }

    void OnMouseExit() {
        outline.enabled = false;
        flaggedForRemoval = false;
        showInfo = false;
    }

    IEnumerator JumpOnboard() {
        if (animator)
            animator.SetTrigger("JumpOnBoard");
        Vector2 startPosition = transform.position;
        Instantiate(waterSplash, startPosition, Quaternion.identity);
        float jumpProgress = 0;

        while (jumpProgress < 1) {
            Vector2 jumpHeight = new Vector2(0, jumpCurve.Evaluate(jumpProgress));
            transform.position = Vector2.Lerp(startPosition, slot.transform.position, jumpProgress) + jumpHeight;
            jumpProgress += Time.fixedDeltaTime * 2f;

            if (jumpProgress > 0.5) {
                if (sprite.sortingLayerName != "Default")
                    sprite.sortingLayerName = "Default";
            }

            yield return new WaitForFixedUpdate();
        }

        transform.parent = slot.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        if (slot.transform.localPosition.x > 0) {
            sprite.transform.localScale = new Vector3(-1, 1, 1);

        } else {
            sprite.transform.localScale = new Vector3(1, 1, 1);
        }
        if (planks)
            planks.enabled = false;
        if (animator)
            animator.SetTrigger("OnBoard");
        GetComponent<CharacterSounds>().Land();
        Lifeboat.HitSway(slot.transform.localPosition.x);

        yield return null;
    }

    IEnumerator JumpOffboard() {
        yield return new WaitForSeconds(Random.Range(0.1f,1f));
        Lifeboat.RemoveCrew(this);
        if (animator)
            animator.SetTrigger("OffBoard");
        Lifeboat.HitSway(slot.transform.localPosition.x);
        transform.parent = null;
        Vector2 startPosition = transform.position;
        Vector2 endPosition = new Vector3(Random.Range(-5.0f, 5.0f), -1.0f);

        float jumpProgress = 0;
        
        while (jumpProgress < 1) {
            Vector2 jumpHeight = new Vector2(0, jumpCurve.Evaluate(jumpProgress));
            transform.position = Vector2.Lerp(startPosition, endPosition, jumpProgress) + jumpHeight;
            jumpProgress += Time.fixedDeltaTime;

            if (jumpProgress > 0.5) {
                if (sprite.sortingLayerName != "Foreground")
                    sprite.sortingLayerName = "Foreground";
            }

            yield return new WaitForFixedUpdate();
        }
        Instantiate(waterSplash, endPosition, Quaternion.identity);
        SetStatus(CastawayStatus.Drowned);
        yield return null;
    }
    IEnumerator Drown(){
        float drownTime = 0;
        
        Vector2 startPosition = transform.position;
        Vector2 endPosition = new Vector3(startPosition.x, -5.0f);

        while(drownTime < 1){
            drownTime+= Time.fixedDeltaTime;

            transform.position = Vector2.Lerp(startPosition,endPosition,drownTime);
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
        yield return null;
    }
    void AdvanceTurn() {
        switch (castawayStatus) {
            case CastawayStatus.Swimming:
                currentEnergy--;

                if (currentEnergy < 0) {
                    GameMaster.peopleLeftToDrown++;
                    SetStatus(CastawayStatus.Drowned);
                }
            
                break;
            case CastawayStatus.Rescued:
                if (!resting) {
                    if (consumesEnergyOnboard) {
                        currentEnergy--;
                        if (animator)
                            animator.SetTrigger("Bucket");
                        if (currentEnergy <= 0) {
                            BeginResting();
                        }
                    }
                } else {
                    currentEnergy++;
                    if (currentEnergy >= maxEnergyOnboard) {
                        EndResting();
                    }
                }
            
                break;
        }
    }

    void FixedUpdate() {
        switch (castawayStatus) {
            case CastawayStatus.Swimming:
                if (xTargetPosition == 0) {
                    if (transform.position.x < -6) {
                        xTargetPosition = Random.Range(-6, 0);
                        sprite.transform.localScale = new Vector3(-1, 1, 1);
                    } else if (transform.position.x > 6) {
                        xTargetPosition = Random.Range(0, 6);
                        sprite.transform.localScale = new Vector3(1, 1, 1);
                    }
                } else {
                    Vector2 currentPosition = transform.position;
                    float speed = Mathf.Min(Mathf.Abs((xTargetPosition - currentPosition.x) * 0.8f), swimSpeed);
                    currentPosition.x = Mathf.MoveTowards(currentPosition.x, xTargetPosition, speed * Time.fixedDeltaTime);
                    transform.position = currentPosition;
                }

                energyFill.fillAmount = Mathf.Lerp(((float)(currentEnergy)) / ((float)(maxEnergySwimming)), energyFill.fillAmount, 0.9f);
                energyFill.color = swimmingGradient.Evaluate(energyFill.fillAmount);
                break;
            case CastawayStatus.Rescued:
            
                energyFill.fillAmount = Mathf.Lerp(((float)(currentEnergy)) / ((float)(maxEnergyOnboard)), energyFill.fillAmount, 0.9f);
                if (resting) {
                    energyFill.color = restingGradient.Evaluate(energyFill.fillAmount);
                } else {
                    energyFill.color = workingGradient.Evaluate(energyFill.fillAmount);
                }
                break;
        }
        
        if (nameDisplay.text != name)
            nameDisplay.text = name;

        if (showInfo) {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1, Time.fixedDeltaTime);
        } else {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0, Time.fixedDeltaTime);
        }
    }

    void BeginResting() {
        resting = true;
        if (animator)
            animator.SetTrigger("Tired");
    }

    void EndResting() {
        restedTimes++;
        if(restedTimes > GameMaster.maxRestTimes){
            GameMaster.maxRestTimes = restedTimes;
        }
        resting = false;
    }

    public void AbandonShip() {
        SetStatus(CastawayStatus.Rejected);
    }

    void SetStatus(CastawayStatus status) {
        castawayStatus = status;

        switch (castawayStatus) {
            case CastawayStatus.Rescued:
                currentEnergy = Mathf.Min(currentEnergy, maxEnergyOnboard - 1);
                slot = Lifeboat.AddCrew(this);
                resting = true;
                StartCoroutine(JumpOnboard());
                break;
            case CastawayStatus.Rejected:
                StartCoroutine(JumpOffboard());
                break;
            case CastawayStatus.Drowned:
                StartCoroutine(Drown());
                break;
        }
    }

    public static void UpdateTurn() {
        Castaway[] castaways = FindObjectsOfType<Castaway>();

        foreach (Castaway c in castaways) {
            c.AdvanceTurn();
        }
    }


    public void GivePlanks() {
        if (planks) {
            planks.enabled = true;
            carriesPlanks = true;
        }
    }

    public enum CastawayStatus {
        Swimming,
        Rescued,
        Rejected,
        Drowned
    }
}

