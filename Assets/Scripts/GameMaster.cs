﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameMaster : MonoBehaviour {

    public CanvasGroup introSplash;
    public CanvasGroup endSplash;
    public Text endStats;
    GameMode gameMode;
    float endScreenCountDown;
    static GameMaster instance;

    void Awake() {
        instance = this;
        Input.simulateMouseWithTouches = true;
    }

    void Update() {
        switch (gameMode) {
            case GameMode.Intro:
                introSplash.alpha = Mathf.MoveTowards(introSplash.alpha, 1, Time.deltaTime * 2);
                endSplash.alpha = Mathf.MoveTowards(endSplash.alpha, 0, Time.deltaTime * 2);
                if (Input.anyKeyDown) {
                    Music.PlayMusic();
                    SideSpawner.FirstSpawn();
                    Lifeboat.StartGame();
                    endScreenCountDown = 5;
                    ResetStatics();
                    gameMode = GameMode.Gameplay;
                }
                break;
            case GameMode.Gameplay:
                introSplash.alpha = Mathf.MoveTowards(introSplash.alpha, 0, Time.deltaTime * 2);
                endSplash.alpha = Mathf.MoveTowards(endSplash.alpha, 0, Time.deltaTime * 2);
                if (Lifeboat.IsSinked) {
                    endScreenCountDown -= Time.deltaTime;

                    if (endScreenCountDown <= 0) {
                        FillStats();
                        Music.FadeStop();
                        gameMode = GameMode.EndScreen;
                    }
                }
                break;
            case GameMode.EndScreen:
                introSplash.alpha = Mathf.MoveTowards(introSplash.alpha, 0, Time.deltaTime * 2);
                
                endSplash.alpha = Mathf.MoveTowards(endSplash.alpha, 1, Time.deltaTime * 2);

                if (Input.anyKeyDown) {
                    gameMode = GameMode.Intro;
                }
                break;
        }
    }

    void ResetStatics() {
        peopleRescued = 0;
        peopleMadeItToEnd = 0;
        peopleKicked = 0;
        peopleLeftToDrown = 0;
        maxRestTimes = 0;
        timeSurvived = 0;
    }

    void FillStats(){
        string stats = String.Format("[SURVIVED\t\t\t\t\n\t\tfor {0} seconds]\n\n",timeSurvived.ToString("N2"));
        stats+= String.Format("[PEOPLE rescued:\t{0}]\n",peopleRescued);
        stats+= String.Format("[PEOPLE sacrificed:\t{0}]\n",peopleKicked);
        stats+= String.Format("[PEOPLE left to drown:\t{0}]\n",peopleLeftToDrown);
        stats+= String.Format("[PEOPLE who made it to the end:\t{0}]\n",peopleMadeItToEnd);
        stats+= String.Format("[MOST times you let SOMEONE rest:\t{0}]\n",maxRestTimes);
        endStats.text = stats;
    }
    public static int peopleRescued = 0;
    public static int peopleMadeItToEnd = 0;
    public static int peopleKicked = 0;
    public static int peopleLeftToDrown = 0;
    public static int maxRestTimes = 0;
    public static float timeSurvived = 0;

    public static GameMode currentGameMode { get { return instance.gameMode; } }

    public enum GameMode {
        Intro,
        Gameplay,
        EndScreen
    }
}
