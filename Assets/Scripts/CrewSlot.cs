﻿using UnityEngine;
using System.Collections;

public class CrewSlot : MonoBehaviour {

    public bool free = true;
    public Castaway crewMember;

    public void SetMember(Castaway member) {
        crewMember = member;
        free = false;
    }

    public void ClearIfMatches(Castaway member) {
        if (member == crewMember) {

            crewMember = null;
            free = true;
        }
    }

    public void Clear() {
        crewMember = null;
        free = true;
    }

    void OnDrawGizmos(){
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position,0.3f);
    }
}
