﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Lifeboat : MonoBehaviour {

    [Header("Control")]
    public int
        room;
    public int maxTotalWeight;
    public int inflowGainTurns;
    public int plankInflowReduction;
    public float turnSpan;
    public float planksCooldown;
    [Header("Status")]
    public bool
        sinked;
    public float playTime;
    public float currentTurnSpan;
    public int inflowGainTurnsLeft;
    public int currentTurn;
    public int waterInflow;
    public int waterOutflow;
    public int waterLevel;
    public int totalWeight;
    public List<Castaway> currentCrew;
    public CrewSlot[] crewSlots;
    public float plankCooldownLeft;
    [Header("Graphics")]
    public GameObject[]
        cracks;
    [Header("Audio")]
    public AudioSource
        dripping;
    public AudioSource streaming;
    [Header("UI")]
    public Image
        waterLevelIndicator;
    Animator animator;
    
    void Awake() {
        currentCrew = new List<Castaway>();

        crewSlots = FindObjectsOfType<CrewSlot>();

        instance = this;
        animator = GetComponent<Animator>();
    }

    void Reset() {
        lastPosition = Vector3.zero;
        plankCooldownLeft = planksCooldown;
        playTime = 0;
        currentTurn = 0;
        currentTurnSpan = 0;
        waterLevel = 0;
        waterOutflow = 0;
        waterInflow = 1;
        inflowGainTurnsLeft = inflowGainTurns;
        for (int crack = 0; crack <cracks.Length; crack++) {
            cracks [crack].SetActive(crack < waterInflow / 3);
        }

        dripping.volume = 0;
        streaming.volume = 0;

        sinked = false;
    }

    void AdvanceTurn() {
        inflowGainTurnsLeft--;
        if (inflowGainTurnsLeft <= 0) {
            waterInflow++;
            inflowGainTurnsLeft = inflowGainTurns;
            for (int crack = 0; crack <cracks.Length; crack++) {
                cracks [crack].SetActive(crack < waterInflow / 3);
            }
        }
        waterOutflow = 0;
        int crewWeight = 0;
        foreach (Castaway c in currentCrew) {
            if (!c.resting)
                waterOutflow += c.waterRemoval;

            crewWeight += c.weight;
        }
        waterLevel = Mathf.Max(0, waterLevel + waterInflow - waterOutflow);

        totalWeight = waterLevel + crewWeight;
        if (((float)totalWeight) / ((float)maxTotalWeight) > 0.7f && plankCooldownLeft == 0) {
            SideSpawner.SpawnPlanks();
            plankCooldownLeft = planksCooldown;
        }

        if (totalWeight > maxTotalWeight) {
            Sink();
        }

        Castaway.UpdateTurn();

        waterLevelIndicator.fillAmount = ((float)totalWeight) / ((float)maxTotalWeight);
        currentTurn++;
        currentTurnSpan -= turnSpan;
    }

    void Sink() {
        GameMaster.timeSurvived = playTime;
        GameMaster.peopleMadeItToEnd = currentCrew.Count;
        foreach (Castaway c in currentCrew) {
            c.AbandonShip();
        }
        sinked = true;
    }

    void FixedUpdate() {
        dripping.volume = Mathf.MoveTowards(dripping.volume, 1.0f - ((float)waterInflow) / 10, Time.fixedDeltaTime);
        streaming.volume = Mathf.MoveTowards(streaming.volume, ((float)waterInflow) / 10, Time.fixedDeltaTime);


        if (GameMaster.currentGameMode == GameMaster.GameMode.Gameplay) {
            playTime += Time.fixedDeltaTime;
            currentTurnSpan += Time.fixedDeltaTime;
            plankCooldownLeft = Mathf.Max(0, plankCooldownLeft - Time.fixedDeltaTime);

            if (!sinked && Input.GetKeyDown(KeyCode.Q)) {
                Sink();
            }

            if (!sinked) {
                if (currentTurnSpan >= turnSpan) {
                    AdvanceTurn();
                }
            }
        }
    }

    Vector3 lastPosition;

    void LateUpdate() {
        if (!sinked) {
            lastPosition = Vector3.Lerp(new Vector3(0, ((float)-totalWeight) / ((float)maxTotalWeight) * 2, 0), lastPosition, 0.98f);
            transform.position += lastPosition;
        } else {
            lastPosition += Vector3.down * Time.fixedDeltaTime * 2;
            transform.position += lastPosition;
        }
    }

    static Lifeboat instance;

    public static int RoomLeft { 
        get { 
            int currentUsedRoom = 0;
            foreach (Castaway c in instance.currentCrew) {
                currentUsedRoom += c.roomRequirement;
            }
            return instance.room - currentUsedRoom;
        } 
    }
    
    static CrewSlot FindEmptySlot() {
        for (int s = 0; s < instance.crewSlots.Length; s++) {
            if (instance.crewSlots [s].free) {
                return instance.crewSlots [s];
            }
        }
        return null;
    }

    public static void HitSway(float X) {
        instance.animator.SetFloat("HitSide", X);
        instance.animator.SetTrigger("Hit");
    }

    /// <summary>
    /// Adds a new member to the crew.
    /// </summary>
    /// <returns>The crew slot.</returns>
    /// <param name="member">Member.</param>
    public static CrewSlot AddCrew(Castaway member) {
        instance.currentCrew.Add(member);
        CrewSlot slot = FindEmptySlot();
        if (member.carriesPlanks) {
            instance.waterInflow -= instance.plankInflowReduction;
        }
        slot.SetMember(member);
        return slot;
    }


    /// <summary>
    /// Removes the member from the crew.
    /// </summary>
    /// <param name="member">Member.</param>
    public static void RemoveCrew(Castaway member) {
        foreach (CrewSlot slot in instance.crewSlots) {
            slot.ClearIfMatches(member);
        }
        instance.currentCrew.Remove(member);
    }

    public static void StartGame() {
        instance.Reset();
    }

    public static bool IsSinked { get { return instance.sinked; } }
}
