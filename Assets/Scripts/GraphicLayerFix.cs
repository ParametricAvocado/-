﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR

[ExecuteInEditMode]

#endif
public class GraphicLayerFix : MonoBehaviour {

    public string sortingLayer;
    public int sortingOrder;
    void Start()
    {
        Sort();
    }
#if UNITY_EDITOR
	void Update () {
        Sort();
	}
#endif
    void Sort()
    {
        if (renderer != null)
        {
            renderer.sortingLayerName = sortingLayer;
            renderer.sortingOrder = sortingOrder;
        }
        if (particleSystem != null)
        {
            particleSystem.renderer.sortingLayerName = sortingLayer;
            particleSystem.renderer.sortingOrder = sortingOrder;
        }
        
    }
}
