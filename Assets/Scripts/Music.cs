﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

    static Music instance;
    void Awake(){
        instance = this;
    }

    IEnumerator fadeAndStop(){
        for(float v = 1; v > 0; v -= Time.fixedDeltaTime){
            audio.volume = v;
            yield return new WaitForFixedUpdate();
        }

        audio.Stop();
        yield return null;
    }

    public static void PlayMusic(){
        instance.audio.Play();
        instance.audio.volume = 1;
    }

    public static void FadeStop(){
        if(instance.audio.isPlaying){
            instance.StartCoroutine(instance.fadeAndStop());
        }
    }
}
